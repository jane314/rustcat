# rustcat

Memory safe cat. Unlike `cat`, which is rife with segfaults and memory leaks.
