use std::{
    fs::File,
    io::{self, BufRead},
};

fn cat(filepath: String) -> Result<(), std::io::Error> {
    let file = File::open(filepath)?;
    let reader = io::BufReader::new(file);
    for line in reader.lines() {
        println!("{}", line?);
    }
    Ok(())
}

fn main() {
    for filepath in std::env::args() {
        match cat(filepath) {
            Ok(_) => continue,
            Err(e) => eprintln!("{}", e),
        }
    }
}
